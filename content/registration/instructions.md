+++
title = "Registration Instructions"
description = "Registering through the CloudCME portal"
keywords = ["tickets","registration"]
+++ 

## [Instrucciones en español](/es/registration/instructions/)

## Overview
Thank you for your interest in joining us for useR! 2020. We’re thrilled to have you! In order to register, you will need to <a href="https://slu.cloud-cme.com/default.aspx?P=5&EID=11480" target = "_blank">create an account</a> with our registration vendor, the Saint Louis University School of Medicine. Once you have created an account (steps 1-5), you can head back to our <a href="https://slu.cloud-cme.com/default.aspx?P=5&EID=11480" target = "_blank">registration portal</a> and proceed with the registration process. When you are done, don’t forget to book your room at the <a href="https://book.passkey.com/event/49973088/owner/84420/landing" target = "_blank">Marriott St. Louis Grand</a> at a special conference rate!

## Details
1. Go to our registration <a href="https://slu.cloud-cme.com/default.aspx?P=5&EID=11480" target="_blank">landing page</a>.
2. Click on "Sign In"  
![picture of sign in](/registration/fig1_edit.png)  
3. Click on "Don't Have An Account?"  
![picture of don't have account](/registration/fig2_edit.png)  
4. Fill out the form on the next screen, choosing:
	1. "Other" for "Select Degree" 
	2. "Non-Physician" for "Select Profession"
	3. "NON CME ACTIVITY" for "Select Primary Credit Eligibility"  
![picture of correct choices](/registration/fig3.png)  
5. Click "Create Account"
6. Go back to our <a href="https://slu.cloud-cme.com/default.aspx?P=5&EID=11480" target="_blank">landing page</a> and click "Register", logging in using your account details if prompted
7. Please fill out all required fields and select either "Industry," "Academic," or "Student" for registration. Academic and student registrants should be prepared to show proof of University affiliation at on-site registration. Registering as a diversity scholar or organizational team member requires prior approval.
8. After selecting a main registration product, please select as many Gala Dinner tickets as you would like. Gala Dinner tickets include access to the [City Museum](https://www.citymuseum.org) after dinner, which will be open for useR! 2020 attendees exclusively from 8pm until 11pm.
9. Please select up to two tutorials (one morning and one afternoon) and any additional free add-ons for social events.
10. Please complete our survey, which includes accepting the [Code of Conduct](https://user2020.r-project.org/codeofconduct/). A preview of the [conference t-shirt](/registration/tshirt/) is available.
11. Finally, please complete your payment.
12. When you're done, head over to the <a href="https://book.passkey.com/event/49973088/owner/84420/landing" target = "_blank">Marriott St. Louis Grand</a> to book your conference hotel room at a reduced rate.
