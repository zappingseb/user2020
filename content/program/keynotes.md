+++
title = "Keynote Speakers"
date = "2020-01-25T00:00:00"
tags = ["Keynotes", "Speakers"]
categories = ["Keynotes"]
+++

## Amelia McNamara, Ph.D.

Amelia McNamara is an assistant professor in the Department of Computer & Information Sciences at the University of St Thomas, in Minnesota.  She received her PhD in statistics from UCLA. Her research lies at the intersection of statistics education and statistical computing, with the goal of making it easier for everyone to learn and do statistics. She has been involved in the development of R packages such as mobilizr (for high school students learning data science) and skimr (frictionless summary statistics). 

{{< figure src="/news/amelia.png" caption="Amelia McNamara" >}}

## Anna Krystalli, Ph.D.

Anna is a Research Software Engineer (RSE) at the University of Sheffield with a background in Marine Macroecology and research software development in R. She is part of a team of RSEs working to help researchers build more robust analysis pipelines and software, promote best practice in research programming and digital resource management and facilitate the shift to more open, transparent and collaborative research culture.  She is also an editor for rOpenSci, a 2019 Software Sustainability Institute Fellow and a co-organiser of the Sheffield R Users group. Overall, her passion lies in helping researchers and the research community as a whole make better use of the real workhorses of research, code and data, and in spreading the word about the joys of R.

{{< figure src="/news/anna_krystalli.jpg" caption="Anna Krystalli" >}}

## Erin LeDell, Ph.D.

Erin LeDell is the Chief Machine Learning Scientist at H2O.ai, the company that produces the open source, distributed machine learning platform, H2O. She received her Ph.D. from UC Berkeley where her research focused on machine learning and computational statistics. Erin is the founder of Women in Machine Learning & Data Science (WiMLDS), and one of the co-founders of R-Ladies Global. Erin is also an active developer of a number of R packages, including `h2o` , `cvAUC`, and `rsparkling`.

{{< figure src="/news/erin.png" caption="Erin LeDell" >}}

## Martin Mächler, Ph.D.

Martin Mächler is the Secretary General of the R Foundation and a member of the R Core Development team. He is a Senior Scientist and Lecturer at ETH Zurich's Seminar for Statistics. Martin is also an active developer of over twenty R packages, including `Matrix` and `cluster`.

{{< figure src="/news/martin.png" caption="Martin Mächler" >}}

## Noam Ross, Ph.D.

Noam Ross is a Senior Research Scientist and computational disease ecologist at EcoHealth Alliance. He received his Ph.D. from UC Davis, is a founding editor for software review at ROpenSci, and is an instructor for the Software Carpentry and Data Carpentry foundations. Noam is also an active developer of a number of R packages, including `fasterize` and `redoc`.

{{< figure src="/news/noam.png" caption="Noam Ross" >}}

## Przemysław Biecek, Ph.D.

Przemysław Biecek is an Associate Professor at Warsaw University of Technolog and Principal Data Scientist at Samsung R&D Institute Poland. Przemek graduated in mathematical statistics and software engineering. Then he fell in love with R. Since then he has been working on predictive models mostly with applications in oncology.  In such applications we need to understand models that are in use and that is why his current obsession is to visualize, explore and debug models. In his free time Przemek is a big believer of data-literacy. He has written four books about R, data visualisation and statistical modeling. Recently he started a Beta-Bit project with data-oriented games, which takes place in R console. Look for the `BetaBit` package on CRAN.

{{< figure src="/news/przemek.png" caption="Przemysław Biecek" >}}
