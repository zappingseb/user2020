# Guidelines

## Deadline for submission is June 20th, 2020.  
* An [starting example template](https://gitlab.com/R-conferences/usertemplate/-/tree/master/example) can be found on the GitLab site.
* We have also created a [xaringan template](https://gitlab.com/R-conferences/usertemplate) that implements several of the recommendations found below that you may wish to use.

## Technical requirements
*   **Length**
    *   contributed: 10-15 minutes
    *   lightning: 3-5 minutes
    *   poster: 1-3 minutes
*   **Video should be picture in picture** (slides in the background, speaker smaller in the foreground), or slides and speaker should be on splitted screens.
*   We suggest the following tools:
    *   Zoom (share only the slides)
    *   Kazam
    *   Simple Screen Recorder
    *   OBS Studio
*   Please let us know if your video requires a content warning (e.g. due to unavoidable presentation of offensive or sexualised material, or due to flashing images).
*   Submit your presentation/poster in markdown as it will enable blind people to use screen reading software or braille displays to follow the details (e.g. code) of talks.
*   **Submit your video in one of these formats:**
    *   .MOV
    *   .MPEG4
    *   .MP4
    *   .AVI
    *   .WMV
    *   .FLV

## Video of the speaker
*   Best would be a neutral background and low or no background audio.
*   Note that virtual backgrounds can create flicker - test first to check.
*   **Your face should be clearly visible** (people who are deaf or hard of hearing may be able to lip-read).
*   Speak clearly.
*   Please pace yourself, so the audience can integrate both audio and visual information. Graphics, pictures, videos, and memes should be described audibly.
*   Test your audio and video beforehand to see whether everything works properly.
*   Speak every word on a slide, read long excerpts aloud.
*   Verbally describe images.

## Slides
*   Use large sans serif fonts (as a guide 28-32pt or above for regular text).
    *   This applies also to code. Use fewer characters per line than you normally would, e.g. max 50 characters per line of code.
*   Use high contrast (you can use [https://contrastchecker.com/](https://contrastchecker.com/) to check).
    *   Consider the theme used for code blocks/screenshots.
*   Make sure slides are discernable for color blind users (you can use [https://www.color-blindness.com/coblis-color-blindness-simulator/](https://www.color-blindness.com/coblis-color-blindness-simulator/) to test screenshots and [https://wave.webaim.org/](https://wave.webaim.org/) to test files in the browser).
*   Use more than color to communicate information (color coding cannot be understood by people who are blind or colorblind).
*   In addition to verbal emphasis, one could use bold, italic, underline, *asterisks*, etc. to convey emphasis.
*   Keep text brief.
*   Make graphics simple.
*   Do not use flashing videos or images.
*   Avoid using animations (Unless with a detailed audio description).
*   Avoid images of text.
*   Provide a text equivalent for graphics, but not for graphics that are only meant for decoration.

## Downloadable resources

*   Submit your presentation/poster in markdown as it will enable blind people to use screen reading software or braille displays to follow the details (e.g. code) of talks.
    *   Latex is an acceptable alternative if you have equations in your presentation, and don’t want to convert to markdown.
    *   Ppt and pdf are the worst if you have code or equations in the text.
*   Use large sans serif fonts (as a guide 28-32pt or above for regular text)
    *   This applies also to code. Use fewer characters per line than you normally would, e.g. max 50 characters per line of code
*   Use high contrast (you can use [https://contrastchecker.com/](https://contrastchecker.com/) to check).
    *   Consider the theme used for code blocks/screenshots.
*   Make sure slides are discernable for color blind users (you can use [https://www.color-blindness.com/coblis-color-blindness-simulator/](https://www.color-blindness.com/coblis-color-blindness-simulator/) to test screenshots and [https://wave.webaim.org/](https://wave.webaim.org/) to test files in browser).
*   Use more than color to communicate information (color coding cannot be understood by people who are blind or colorblind).
*   Consider submitting a simple copy of your talk with large print so audiences can follow along on screen or print out.

*   Add alt-text descriptions for graphics, images, memes, screenshots, and other graphically-presented material.

Further recommendations can be found on the Perkins School for the Blind website: [https://www.perkinselearning.org/technology/digital-transitions/creating-accessible-powerpoint-presentations-students-visual](https://www.perkinselearning.org/technology/digital-transitions/creating-accessible-powerpoint-presentations-students-visual) and on SIG ACCESS:

*   for presentations [http://www.sigaccess.org/welcome-to-sigaccess/resources/accessible-presentation-guide/](http://www.sigaccess.org/welcome-to-sigaccess/resources/accessible-presentation-guide/) 
*   for PDFs [http://www.sigaccess.org/welcome-to-sigaccess/resources/accessible-pdf-author-guide/](http://www.sigaccess.org/welcome-to-sigaccess/resources/accessible-pdf-author-guide/)

All speakers are reminded to review the [Code of Conduct](https://user2020.r-project.org/codeofconduct/) when constructing your presentation.
