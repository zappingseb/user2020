## useR! 2020 is now a virtual conference.  

### Agenda
- Keynotes will stream live online
- Contributed talks and posters will be presented as YouTube videos
- Some [tutorials](/program/tutorials/) are still planned and will be live

The keynotes and talks do no require sign up, they will be free and open to all.

A procedure for signing up for the tutorials will be shared shortly.
