+++
title = "Tutorial submissions are now open!"
date = "2019-10-23T13:10:00"
tags = ["Tutorials"]
categories = ["Tutorials"]
banner = "news/tutorials-now-open.png"
+++

# Tutorial submissions are now open!

Following up on our previous [Call for Tutorials](https://user2020.r-project.org/news/2019/09/28/call-for-tutorials/), we are happy to announce that the submission page is now live. [Head over there](https://user2020.secure-platform.com/) to pitch your idea. Most users will need to create an account before submitting. The deadline for submission is December 2, 2019, 11:59 PM UTC. Further questions about tutorial topics or the submission process should be addressed to [Srikanth Mudigonda](mailto:srikanth.mudigonda@slu.edu).