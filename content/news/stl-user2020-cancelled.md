+++
title = "user!2020 in St. Louis cancelled"
date = "2020-04-17T22:00:00"
banner = "news/user2020-stl.png"
+++

We have made the disappointing decision to cancel useR! 2020 in St. Louis. This has been in process for several weeks, but we have not been able to share details publicly until today. We've appreciated the R community's patience as we've worked on this.

We have not made any final decisions about what comes next for #useR2020. We’ll be working with the R Foundation, useR! 2020 Munich, and our local team next week to determine the best course of action for us to collectively take. We’ll be in touch soon about possible next steps.

If you have already registered for the conference, you can expect to hear from us early next week about refunds associated with your registration. Our sponsors, keynotes, tutorial leaders, presenters, and diversity scholars will be receiving updates from our team as well. If you have registered for a room with our venue, the Marriott St. Louis Grand, the hotel will be cancelling those reservations.

In the meantime, we hope our entire R community remains healthy and well. We also want to thank the nearly 60 folks who have contributed to our organizing team and  spent countless hours working towards a successful conference. Your hard work means the world to us.